module.exports = {
  stories: ['../src/**/*.stories.(tsx|mdx)'],
  addons: ['storybook-addon-styled-component-theme/dist/register'],
};
