import { addDecorator } from '@storybook/react';
import { withThemesProvider } from 'storybook-addon-styled-component-theme';
import { lightTheme, darkTheme } from '../src/theme';

const themes = [lightTheme, darkTheme];
addDecorator(withThemesProvider(themes));
