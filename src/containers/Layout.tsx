import React from 'react';
import { styled } from '../theme';

// Components
import Header from '../components/Header';
import ScrollTop from '../components/ScrollTop';
import Footer from '../components/Footer';

const LayoutWrapper = styled.div`
  background-color: ${(props) => props.theme.colors.bgPrimary};
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    padding-top: 52px;
  }
`;

const Resopnsive = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  background-color: ${(props) => props.theme.colors.bgPrimary};
  margin: auto;
  ${(props) => props.theme.mediaQueries.sm} {
    max-width: 500px;
  }
  ${(props) => props.theme.mediaQueries.md} {
    max-width: 700px;
  }
  ${(props) => props.theme.mediaQueries.lg} {
    max-width: 900px;
  }
  ${(props) => props.theme.mediaQueries.xl} {
    max-width: 1000px;
  }
`;

const Title = styled.div`
  background-color: ${(props) => props.theme.colors.bgTitle};
  border-top-color: #eeeeee;
  border-bottom-color: #eeeeee;
  display: block;
  position: relative;
  border-top: 2px solid #f2f2f2;
  border-bottom: 2px solid #f2f2f2;
  padding: 40px;
  margin-bottom: 30px;
  text-align: left;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    padding: 15px 35px;
  }
`;

const H1 = styled.h1`
  color: ${(props) => props.theme.colors.textPrimary};
  font-size: 35px;
  font-weight: 600;
  font-style: normal;
  letter-spacing: 0px;
  margin: 0;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    font-size: 25px;
  }
`;

const SubTitle = styled.h4`
  display: inline-block;
  position: absolute;
  top: 0;
  right: 70px;
  color: #aaaaaa;
  font-size: 14px;
  font-weight: 300;
  font-style: normal;
  letter-spacing: 0px;
  margin: 1.33em 0;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    position: relative;
    right: auto;
    margin: 5px 0;
  }
`;

type Props = {
  children: React.ReactNode;
  title?: string;
  subTitle?: string;
};

const Layout: React.FunctionComponent<Props> = ({
  children,
  title,
  subTitle,
}) => {
  return (
    <LayoutWrapper>
      <Header />
      {title && (
        <Title>
          <H1>{title}</H1>
          {subTitle && <SubTitle>{subTitle}</SubTitle>}
        </Title>
      )}
      <Resopnsive>
        {children}
        <Footer />
      </Resopnsive>
      <ScrollTop />
    </LayoutWrapper>
  );
};

export default Layout;
