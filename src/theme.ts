import baseStyled, { ThemedStyledInterface } from 'styled-components';

const breakpoints = ['576px', '768px', '992px', '1200px'];

const defaultTheme = {
  breakpoints,
  space: [0, 4, 8, 16, 32, 64, 128, 256],
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  mediaQueries: {
    sm: `@media screen and (min-width: ${breakpoints[0]})`,
    md: `@media screen and (min-width: ${breakpoints[1]})`,
    lg: `@media screen and (min-width: ${breakpoints[2]})`,
    xl: `@media screen and (min-width: ${breakpoints[4]})`,
  },
};

const light = {
  name: 'Light',
  colors: {
    primary: '#54CA95',
    secondary: '#D5D5D5',
    white: '#FFFFFF',
    bgPrimary: '#FFFFFF',
    bgTimeLine: '#FFFFFF',
    textPrimary: '#333333',
    textTimeLine: '#666666',
    bgTitle: '#fcfcfc',
    black: '#333333',
    light: '#F5F5F5',
    shadow: 'rgba(0, 0, 0, 0.1)',
  },
  variants: {},
};

const dark = {
  name: 'Dark',
  colors: {
    primary: '#54CA95',
    secondary: '#4D4D4D',
    white: '#FFFFFF',
    bgPrimary: '#18191a',
    bgTimeLine: '#232626',
    textPrimary: '#FFFFFF',
    textTimeLine: '#FFFFFF',
    bgTitle: '#313131',
    black: '#333333',
    light: '#F5F5F5',
    shadow: 'rgba(0, 0, 0, 0.5)',
  },
  variants: {},
};

export const lightTheme = { ...defaultTheme, ...light };
export const darkTheme = { ...defaultTheme, ...dark };

export type Theme = typeof lightTheme | typeof darkTheme;
export const styled = baseStyled as ThemedStyledInterface<Theme>;
