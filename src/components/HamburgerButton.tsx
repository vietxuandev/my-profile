import * as React from 'react';
import { styled } from '../theme';

type Props = {
  open: boolean;
  setOpen: () => void;
};

type ButtonProps = {
  open: boolean;
};

const HamburgerItem = styled.span`
  background-color: ${(props) => props.theme.colors.textPrimary};
  position: absolute;
  height: 3px;
  width: 100%;
  border-radius: 9px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: 0.25s ease-in-out;
  -moz-transition: 0.25s ease-in-out;
  -o-transition: 0.25s ease-in-out;
  transition: 0.25s ease-in-out;
`;

const HamburgerButtonWrapper = styled.div<ButtonProps>`
  position: relative;
  width: 24px;
  height: 24px;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
  cursor: pointer;

  ${HamburgerItem}:nth-of-type(1) {
    top: 0px;
    -webkit-transform-origin: left center;
    -moz-transform-origin: left center;
    -o-transform-origin: left center;
    transform-origin: left center;
    ${(props: ButtonProps) =>
      props.open &&
      `-webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        left: 3px;`}
  }

  ${HamburgerItem}:nth-of-type(2) {
    top: 7px;
    -webkit-transform-origin: left center;
    -moz-transform-origin: left center;
    -o-transform-origin: left center;
    transform-origin: left center;
    ${(props: ButtonProps) =>
      props.open &&
      `width: 0%;
      opacity: 0;`}
  }

  ${HamburgerItem}:nth-of-type(3) {
    top: 14px;
    -webkit-transform-origin: left center;
    -moz-transform-origin: left center;
    -o-transform-origin: left center;
    transform-origin: left center;
    ${(props: ButtonProps) =>
      props.open &&
      `-webkit-transform: rotate(-45deg);
      -moz-transform: rotate(-45deg);
      -o-transform: rotate(-45deg);
      transform: rotate(-45deg);
      top: 17px;
      left: 3px;`}
  }
`;

const HamburgerButton: React.FunctionComponent<Props> = ({
  open,
  setOpen,
}: Props) => {
  return (
    <HamburgerButtonWrapper onClick={setOpen} open={open}>
      <HamburgerItem />
      <HamburgerItem />
      <HamburgerItem />
    </HamburgerButtonWrapper>
  );
};

export default HamburgerButton;
