import React from 'react';
import { styled } from '../theme';

const ContactWrapper = styled.div`
  position: relative;
  text-align: left;
  width: 100%;
  display: table;
  margin: 0;
  padding: 0 10px 30px 0;
`;

const ContactIcon = styled.div`
  position: relative;
  display: table-cell;
  padding: 0 10px 5px 0;
  width: 44px;
  i {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    font-size: 33px;
    color: ${(props) => props.theme.colors.primary};
  }
`;

const ContactText = styled.div`
  position: relative;
  display: table-cell;
  padding: 0 0 0 15px;
  vertical-align: middle;
`;

const ContactH4 = styled.h4`
  font-size: 16px;
  color: ${(props) => props.theme.colors.textPrimary};
  margin: 7px 0;
  font-weight: 600;
  font-style: normal;
`;

const ContactP = styled.p`
  font-size: 0.92em;
  font-weight: 400;
  font-style: normal;
  line-height: 1.75em;
  color: ${(props) => props.theme.colors.textTimeLine};
  margin-top: 0;
  margin-bottom: 1rem;
`;

type Props = {
  icon: string;
  name: string;
  value: string;
};

const ContactItem: React.FunctionComponent<Props> = ({ icon, name, value }) => {
  return (
    <ContactWrapper>
      <ContactIcon>
        <i className={icon} />
      </ContactIcon>
      <ContactText>
        <ContactH4>{name}</ContactH4>
        <ContactP>{value}</ContactP>
      </ContactText>
    </ContactWrapper>
  );
};

export default ContactItem;
