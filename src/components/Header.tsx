import * as React from 'react';
import { styled } from '../../src/theme';
import useDarkMode from 'use-dark-mode';
import { WithTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { i18n, Link, withTranslation } from '../../i18n';
import useOnClickOutside from 'use-onclickoutside';

//Images
import ViIcon from '../assets/images/icons/vi.svg';
import EnIcon from '../assets/images/icons/en.svg';

//Components
import HamburgerButton from './HamburgerButton';
import ToggleButton from './ToggleButton';

const items = [
  { id: 1, value: 'about' },
  { id: 2, value: 'resume' },
  { id: 3, value: 'contact' },
];

const HeaderWrapper = styled.div`
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    box-shadow: 0 0 10px 0 ${(props) => props.theme.colors.shadow};
    z-index: 999;
  }
`;

const HeaderInner = styled.div`
  display: flex;
  align-items: center;
  padding: 40px 40px 20px 40px;
  background-color: ${(props) => props.theme.colors.bgPrimary};
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    padding: 10px 20px;
  }
`;

const HomeLogo = styled.div`
  border-radius: 100%;
  background-color: ${(props) => props.theme.colors.primary};
  width: 44px;
  height: 44px;
  text-align: center;
  line-height: 44px;
  font-weight: bold;
  color: ${(props) => props.theme.colors.white};
  cursor: pointer;
  font-size: 28px;
  margin-right: 10px;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    width: 32px;
    height: 32px;
    line-height: 32px;
    font-size: 18px;
    margin-right: 8px;
  }
  :hover {
    box-shadow: 0 0 5px 0 ${(props) => props.theme.colors.shadow};
  }
`;

const HomeName = styled.div`
  margin-right: auto;
  line-height: 50px;
  font-size: 20px;
  color: ${(props) => props.theme.colors.textPrimary};
  font-weight: 600;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    line-height: 32px;
    font-size: 16px;
  }
`;

const HomeNameSpan = styled.span`
  font-weight: 400;
`;

type SideMenuProps = {
  openMenu: boolean;
};
const SideMenu = styled.div<SideMenuProps>`
  display: flex;
  position: relative;
  background-color: ${(props) => props.theme.colors.bgPrimary};
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    display: inline-block;
    position: fixed;
    top: 52px;
    left: auto;
    right: ${(props: SideMenuProps) => (props.openMenu ? 0 : '-350px')};
    bottom: 0;
    width: 100%;
    max-width: 350px;
    box-shadow: 0 0 70px 0 ${(props) => props.theme.colors.shadow};
    transition: all 0.33s ease-in-out;
    transition-property: opacity, width, right;
    opacity: ${(props: SideMenuProps) => (props.openMenu ? 1 : 0)};
    z-index: -1;
  }
  @media screen and (max-width: ${(props) => props.theme.breakpoints[1]}) {
    right: ${(props: SideMenuProps) => (props.openMenu ? 0 : '-300px')};
    max-width: 300px;
  }
  @media screen and (max-width: ${(props) => props.theme.breakpoints[0]}) {
    right: ${(props: SideMenuProps) => (props.openMenu ? 0 : '-250px')};
    max-width: 250px;
  } ;
`;

type SideMenuItemProps = {
  isActive: boolean;
};

const SideMenuItem = styled.div<SideMenuItemProps>`
  display: flex;
  align-items: center;
  opacity: ${(props: SideMenuItemProps) => (props.isActive ? 1 : 0.55)};
  font-size: 16px;
  color: ${(props) => props.theme.colors.textPrimary};
  cursor: pointer;
  transition: opacity 0.33s ease-in-out;
  margin-right: 40px;
  :hover {
    opacity: 1;
  }
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    font-size: 15px;
    line-height: 50px;
    margin: 0 30px;
  } ;
`;

const SideMenuTool = styled.div`
  display: flex;
  align-items: center;
  opacity: 0.55;
  font-size: 16px;
  color: ${(props) => props.theme.colors.textPrimary};
  cursor: pointer;
  transition: opacity 0.33s ease-in-out;
  margin-right: 40px;
  :hover {
    opacity: 1;
  }
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    font-size: 15px;
    line-height: 50px;
    margin: 0 30px;
  } ;
`;

const ImageFlag = styled.img`
  height: 20px;
  margin-left: 10px;
`;

const ToggleButtonWrapper = styled.div`
  margin-left: 10px;
  margin-bottom: 3px;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    margin-left: auto;
    line-height: 50px;
  } ;
`;

const HamburgerButtonWrapper = styled.div`
  display: none;
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    display: block;
  } ;
`;

const Icon = styled.i`
  margin-right: 10px;
  font-size: 18px;
`;

const Header = ({ t }: WithTranslation) => {
  const ref = React.useRef();
  const [openMenu, setOpenMenu] = React.useState<boolean>(false);
  const darkMode = useDarkMode(false);
  const router = useRouter();
  const pathName = router.pathname;
  const closeMenu = React.useCallback(() => {
    setOpenMenu(false);
  }, [setOpenMenu]);
  useOnClickOutside(ref, closeMenu);
  const checkActive = React.useCallback(
    (value: string) => {
      if (value === 'about' && pathName === '/') {
        return true;
      }
      return new RegExp(value).test(pathName);
    },
    [pathName]
  );
  return (
    <HeaderWrapper ref={ref}>
      <HeaderInner>
        <Link href="/">
          <HomeLogo onClick={closeMenu}>X</HomeLogo>
        </Link>
        <HomeName>
          Xuan&apos;s <HomeNameSpan>CV</HomeNameSpan>
        </HomeName>
        <SideMenu openMenu={openMenu}>
          {items.map((item) => (
            <Link key={item.id} href={`/${item.value}`}>
              <SideMenuItem
                isActive={checkActive(item.value)}
                onClick={closeMenu}
              >
                {t(item.value)}
              </SideMenuItem>
            </Link>
          ))}
          <SideMenuTool
            onClick={() => {
              i18n.changeLanguage(i18n.language === 'en' ? 'vi' : 'en');
            }}
          >
            {t('language')}
            <ImageFlag
              src={i18n.language === 'vi' ? EnIcon : ViIcon}
              alt="flag"
            />
          </SideMenuTool>
          <SideMenuTool>
            {t('darkMode')}
            <ToggleButtonWrapper>
              <ToggleButton
                checked={darkMode.value}
                onChange={() => darkMode.toggle()}
              />
            </ToggleButtonWrapper>
          </SideMenuTool>
          <Link href={`/signIn`}>
            <SideMenuItem isActive={checkActive('signIn')} onClick={closeMenu}>
              <Icon className="icon-enter" />
              {t('signIn')}
            </SideMenuItem>
          </Link>
        </SideMenu>
        <HamburgerButtonWrapper>
          <HamburgerButton
            open={openMenu}
            setOpen={() => setOpenMenu(!openMenu)}
          />
        </HamburgerButtonWrapper>
      </HeaderInner>
    </HeaderWrapper>
  );
};

Header.getInitialProps = async () => ({
  namespacesRequired: ['header'],
});

export default withTranslation('header')(Header);
