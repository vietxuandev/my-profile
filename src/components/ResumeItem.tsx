import React from 'react';
import { styled } from '../theme';

const TimeLineWrapper = styled.div`
  position: relative;
  padding: 3px 0 20px 25px;
  margin-left: 15px;
  border-left: 1px solid #f5f6f9;
  border-bottom: 1px solid #f5f6f9;
`;

const TimeLine = styled.h5`
  display: inline-block;
  position: relative;
  font-size: 12px;
  font-weight: 500;
  margin: 0 0 5px -40px;
  color: ${(props) => props.theme.colors.textTimeLine};
  background-color: ${(props) => props.theme.colors.bgTimeLine};
  padding: 0 10px;
  line-height: 23px;
  border: 2px solid ${(props) => props.theme.colors.primary};
  border-radius: 30px;
`;

const Company = styled.span`
  font-size: 12px;
  color: ${(props) => props.theme.colors.textTimeLine};
  opacity: 0.7;
  margin-left: 5px;
`;

const Position = styled.h4`
  font-size: 16px;
  margin: 10px 0 7px;
  font-weight: 600;
  font-style: normal;
  color: ${(props) => props.theme.colors.textPrimary};
`;

const Description = styled.p`
  font-size: 0.92em;
  font-weight: 400;
  font-style: normal;
  line-height: 1.75em;
  color: #666666;
  margin-top: 0;
  margin-bottom: 1rem;
`;

type Props = {
  company?: string;
  timeLine?: string;
  position?: string;
  description: { id: number; value: string }[];
};

const ResumeItem: React.FunctionComponent<Props> = ({
  company,
  timeLine,
  position,
  description,
}) => {
  return (
    <TimeLineWrapper>
      {timeLine && <TimeLine>{timeLine}</TimeLine>}
      {company && <Company>{company}</Company>}
      {position && <Position>{position}</Position>}
      {(description ?? []).map((item) => (
        <Description key={item.id}>{item.value}</Description>
      ))}
    </TimeLineWrapper>
  );
};

export default ResumeItem;
