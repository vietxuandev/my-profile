import React from 'react';
import { styled } from '../theme';

const Flex = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  align-items: center;
  border-top: 1px solid rgba(0, 0, 0, 0.2);
  padding: 15px 0;
  margin-top: 10px;
`;

const Box = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  flex: 0 0 100%;
  max-width: 100%;
  text-align: center;
  font-size: 15px;
  line-height: 24px;
  color: #737373;
  ${(props) => props.theme.mediaQueries.md} {
    flex: 0 0 50%;
    max-width: 50%;
    text-align: left;
  }
`;

const SocialIconWrapper = styled.div`
  ${(props) => props.theme.mediaQueries.md} {
    text-align: right;
  }
`;

type hoverColorProps = {
  hoverColor: string;
};

const SocialIcon = styled.a<hoverColorProps>`
  width: 40px;
  height: 40px;
  line-height: 40px;
  margin-left: 6px;
  margin-right: 0;
  border-radius: 100%;
  color: #818a91;
  font-size: 16px;
  display: inline-block;
  text-align: center;
  text-decoration: none;
  transition: background-color 0.2s linear;
  i {
    transition: color 0.2s linear;
    line-height: 40px;
  }
  :hover {
    i {
      color: white;
    }
    background-color: ${(props: hoverColorProps) => props.hoverColor};
  }
`;

const Footer: React.FunctionComponent = () => {
  return (
    <Flex>
      <Box>Copyright © 2020 All Rights Reserved by VietXuanDev.</Box>
      <Box>
        <SocialIconWrapper>
          <SocialIcon hoverColor="#3b5998" href="#">
            <i className="icon-facebook" />
          </SocialIcon>
          <SocialIcon hoverColor="#00aced" href="#">
            <i className="icon-twitter" />
          </SocialIcon>
          <SocialIcon hoverColor="#ea4c89" href="#">
            <i className="icon-dribbble" />
          </SocialIcon>
          <SocialIcon hoverColor="#007bb6" href="#">
            <i className="icon-linkedin" />
          </SocialIcon>
        </SocialIconWrapper>
      </Box>
    </Flex>
  );
};

export default Footer;
