import React from 'react';
import { styled } from '../theme';

const CheckBoxWrapper = styled.div`
  position: relative;
  height: 24px;
`;
const CheckBoxLabel = styled.label`
  position: absolute;
  top: 0;
  left: 0;
  width: 42px;
  height: 24px;
  border-radius: 15px;
  background: ${(props) => props.theme.colors.secondary};
  cursor: pointer;
  &::after {
    content: '';
    display: block;
    border-radius: 50%;
    width: 18px;
    height: 18px;
    margin: 3px;
    background: ${(props) => props.theme.colors.white};
    box-shadow: 1px 3px 3px 1px ${(props) => props.theme.colors.shadow};
    transition: 0.2s;
  }
`;
const CheckBox = styled.input`
  opacity: 0;
  z-index: 1;
  border-radius: 15px;
  width: 42px;
  height: 24px;
  margin: 0;
  &:checked + ${CheckBoxLabel} {
    background: ${(props) => props.theme.colors.primary};
    &::after {
      content: '';
      display: block;
      border-radius: 50%;
      width: 18px;
      height: 18px;
      margin-left: 21px;
      transition: 0.2s;
    }
  }
`;

type Props = {
  checked: boolean;
  onChange: () => void;
};

const ToggleButton: React.FunctionComponent<Props> = ({
  checked,
  onChange,
}) => {
  return (
    <CheckBoxWrapper>
      <CheckBox
        id="checkbox"
        type="checkbox"
        onChange={onChange}
        checked={checked}
      />
      <CheckBoxLabel htmlFor="checkbox" />
    </CheckBoxWrapper>
  );
};

export default ToggleButton;
