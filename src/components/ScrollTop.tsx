import React from 'react';
import { styled } from '../theme';

type Props = {
  showScroll: boolean;
};

const ScrollTTopWrapper = styled.div<Props>`
  position: fixed;
  display: flex;
  height: 50px;
  width: 50px;
  align-items: center;
  justify-content: center;
  bottom: 10px;
  right: 15px;
  z-index: 100;
  visibility: ${(props: Props) => (props.showScroll ? 'visible' : 'hidden')};
  opacity: ${(props: Props) => (props.showScroll ? 1 : 0)};
  transition: all 0.33s ease-in-out;
  transition-property: opacity;
  padding: 0;
  background-color: ${(props) => props.theme.colors.bgPrimary};
  cursor: pointer;
  border: 1px solid ${(props) => props.theme.colors.secondary};
  color: ${(props) => props.theme.colors.textPrimary};
  border-radius: 8px;
  font-size: 25px;
  :hover {
    color: ${(props) => props.theme.colors.primary};
    box-shadow: 0 0 12px 0 ${(props) => props.theme.colors.shadow};
  }
  @media screen and (max-width: ${(props) => props.theme.breakpoints[2]}) {
    height: 40px;
    width: 40px;
    font-size: 20px;
  }
`;

const ScrollTop: React.FunctionComponent = () => {
  const [showScroll, setShowScroll] = React.useState<boolean>(false);
  const logit = () => {
    window.pageYOffset >= 150 ? setShowScroll(true) : setShowScroll(false);
  };
  React.useEffect(() => {
    const watchScroll = () => {
      window.addEventListener('scroll', logit);
    };
    watchScroll();
    return () => {
      window.removeEventListener('scroll', logit);
    };
  });
  return (
    <ScrollTTopWrapper
      showScroll={showScroll}
      onClick={() => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      }}
    >
      <i className="icon-up-arrow" />
    </ScrollTTopWrapper>
  );
};

export default ScrollTop;
