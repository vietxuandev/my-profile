const AUTH_TOKEN = 'auth-token';

export const getToken = (): string => localStorage.getItem(AUTH_TOKEN);
export const setToken = (token: string): void =>
  localStorage.setItem(AUTH_TOKEN, token);
export const deleteToken = (): void => localStorage.removeItem(AUTH_TOKEN);
