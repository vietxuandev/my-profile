import React from 'react';
import { styled } from '../src/theme';

/// Images
import Avatar from '../src/assets/images/avt.png';

//Layouts
import Layout from '../src/containers/Layout';

const AvatarWrapper = styled.div`
  padding: 20px;
`;

const AvatarImage = styled.img`
  box-sizing: border-box;
  margin: 0;
  min-width: 0;
  max-width: 100%;
  height: auto;
  border: 18px solid ${(props) => props.theme.colors.white};
  border-radius: 100%;
  box-shadow: 0 0 25px 0 ${(props) => props.theme.colors.shadow};
`;

const Flex = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  align-items: center;
`;

const Box = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  flex: 0 0 100%;
  max-width: 100%;
  text-align: center;
  ${(props) => props.theme.mediaQueries.md} {
    flex: 0 0 50%;
    max-width: 50%;
    text-align: left;
  }
`;

const Major = styled.h4`
  font-size: 16px;
  color: #aaa;
  font-weight: 300;
  margin-bottom: 10px;
`;

const FullName = styled.h1`
  font-size: 48px;
  line-height: 56px;
  margin-top: 0;
  margin-bottom: 20px;
`;

const Introduce = styled.p`
  font-size: 14px;
  font-weight: 400;
  font-style: normal;
  line-height: 1.75em;
  color: #666666;
  margin-bottom: 5px;
  text-align: left;
`;

const DownloadButton = styled.a`
  display: inline-block;
  position: relative;
  padding: 0.8em 1.8em;
  margin-bottom: 0.75em;
  margin-right: 0.5em;
  font-size: 14px;
  line-height: 1.2;
  border: 0;
  outline: 0;
  border: 2px solid #54ca95;
  color: #666;
  text-shadow: none;
  background-color: #fff;
  border-radius: 30px;
  transition: all 0.3s ease-in-out;
  box-shadow: 0 10px 10px -8px rgba(0, 0, 0, 0.22);
  cursor: pointer;
  :hover {
    color: #fff;
    border-color: #54ca95;
    background-color: #54ca95;
  }
`;

const ViewButton = styled.button`
  display: inline-block;
  position: relative;
  padding: 0.8em 1.8em;
  margin-bottom: 0.75em;
  font-size: 14px;
  line-height: 1.2;
  border: 0;
  outline: 0;
  border: 2px solid #d5d5d5;
  color: #666;
  text-shadow: none;
  background-color: #fff;
  border-radius: 30px;
  font-family: 'Poppins', Helvetica, sans-serif;
  transition: all 0.3s ease-in-out;
  box-shadow: 0 10px 10px -8px rgba(0, 0, 0, 0.22);
  cursor: pointer;
  :hover {
    color: #333;
    border-color: #d5d5d5;
    background-color: #d5d5d5;
  }
`;

const AboutPage: React.FunctionComponent = () => {
  return (
    <Layout>
      <Flex>
        <Box>
          <AvatarWrapper>
            <AvatarImage src={Avatar} alt="Avatar" />
          </AvatarWrapper>
        </Box>
        <Box>
          <Major>Full-stack Javascript</Major>
          <FullName>Xuan Nguyen Viet</FullName>
          <Introduce>
            To apply my knowledge in the field of Full-stack Javascript into
            practical use.
          </Introduce>
          <Introduce>
            To apply my knowledge in the field of Full-stack Javascript into
            practical use.
          </Introduce>
          <Introduce>
            To apply my knowledge in the field of Full-stack Javascript into
            practical use.
          </Introduce>
          <DownloadButton>Download CV</DownloadButton>
          <ViewButton>View Transcript</ViewButton>
        </Box>
      </Flex>
    </Layout>
  );
};

export default AboutPage;
