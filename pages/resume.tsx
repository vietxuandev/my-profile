import * as React from 'react';
import { styled } from '../src/theme';
import { WithTranslation } from 'next-i18next';
import { withTranslation } from '../i18n';

//Layouts
import Layout from '../src/containers/Layout';

// Components
import ResumeItem from '../src/components/ResumeItem';

const Name = styled.h2`
  display: inline-block;
  position: relative;
  font-size: 21px;
  margin: 0 0 30px;
  z-index: 1;
  padding-bottom: 7px;
  font-weight: 600;
  font-style: normal;
  color: ${(props) => props.theme.colors.textPrimary};
  ::before {
    display: block;
    position: absolute;
    content: '';
    width: 100%;
    background-color: ${(props) => props.theme.colors.secondary};
    height: 2px;
    bottom: 0;
  }
  ::after {
    display: block;
    position: absolute;
    content: '';
    width: 30px;
    background-color: ${(props) => props.theme.colors.primary};
    height: 2px;
    bottom: 0;
  }
`;

const Flex = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
`;

const Box = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  flex: 0 0 100%;
  max-width: 100%;
  ${(props) => props.theme.mediaQueries.md} {
    flex: 0 0 50%;
    max-width: 50%;
  }
`;

type Experience = {
  id: number;
  timeLine: string;
  company: string;
  position: string;
  description: { id: number; value: string }[];
};

type Skill = {
  description: { id: number; value: string }[];
};

const ResumePage = ({ t }: WithTranslation) => {
  return (
    <Layout title={t('title')} subTitle={t('subTitle')}>
      <Flex>
        <Box>
          <Name>{t('education.name')}</Name>
          {(
            t<Experience[]>('education.value', { returnObjects: true }) ?? []
          ).map((item) => (
            <ResumeItem
              key={item.id}
              timeLine={item.timeLine}
              company={item.company}
              position={item.position}
              description={item.description}
            />
          ))}
        </Box>
        <Box>
          <Name>{t('experience.name')}</Name>
          {(
            t<Experience[]>('experience.value', { returnObjects: true }) ?? []
          ).map((item) => (
            <ResumeItem
              key={item.id}
              timeLine={item.timeLine}
              company={item.company}
              position={item.position}
              description={item.description}
            />
          ))}
        </Box>
        <Box>
          <Name>{t('skills.name')}</Name>
          <ResumeItem
            description={
              t<Skill>('skills', {
                returnObjects: true,
              }).description
            }
          />
        </Box>
      </Flex>
    </Layout>
  );
};

ResumePage.getInitialProps = async () => ({
  namespacesRequired: ['resume'],
});

export default withTranslation('resume')(ResumePage);
