import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import withApollo from '../lib/withApollo';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { setToken } from '../src/token';
import NProgress from 'nprogress';

//Layouts
import Layout from '../src/containers/Layout';

const SIGNUP = gql`
  mutation SignUpMutation($username: String!, $password: String!) {
    signUp(input: { username: $username, password: $password })
  }
`;

const validationSchema = Yup.object({
  username: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

const SignUpPage = () => {
  const [signUp, { data, loading }] = useMutation<{ signUp: string }>(SIGNUP);
  React.useEffect(() => {
    if (data) {
      setToken(data.signUp);
    }
  }, [data]);
  React.useEffect(() => {
    if (loading) {
      NProgress.start();
    }
    NProgress.done();
  }, [loading]);
  const {
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    errors,
    isValid,
    dirty,
  } = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema,
    onSubmit(values) {
      signUp({ variables: values });
    },
  });
  return (
    <Layout>
      <form onSubmit={handleSubmit}>
        <input
          name="username"
          onChange={handleChange}
          value={values.username}
          onBlur={handleBlur}
        />
        {touched.username && errors.username}
        <input
          name="password"
          onChange={handleChange}
          value={values.password}
          onBlur={handleBlur}
        />
        {touched.password && errors.password}
        <button disabled={!(isValid && dirty)} type="submit">
          Dang nhap
        </button>
      </form>
    </Layout>
  );
};

export default withApollo(SignUpPage);
