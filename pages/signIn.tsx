import React from 'react';
import { useRouter } from 'next/router';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Link } from '../i18n';
import withApollo from '../lib/withApollo';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { setToken } from '../src/token';
import NProgress from 'nprogress';

//Layouts
import Layout from '../src/containers/Layout';

const SIGNIN = gql`
  mutation LoginMutation($username: String!, $password: String!) {
    login(input: { username: $username, password: $password })
  }
`;

const validationSchema = Yup.object({
  username: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

const SignInPage = () => {
  const [onLogin, { data, loading }] = useMutation<{ login: string }>(SIGNIN);
  const router = useRouter();
  React.useEffect(() => {
    if (data) {
      setToken(data.login);
      router.push('/');
    }
  }, [data]);
  React.useEffect(() => {
    if (loading) {
      NProgress.start();
    }
    NProgress.done();
  }, [loading]);
  const {
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    errors,
    isValid,
    dirty,
  } = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema,
    onSubmit(values) {
      onLogin({ variables: values });
    },
  });
  return (
    <Layout>
      <form onSubmit={handleSubmit}>
        <input
          name="username"
          onChange={handleChange}
          value={values.username}
          onBlur={handleBlur}
        />
        {touched.username && errors.username}
        <input
          name="password"
          onChange={handleChange}
          value={values.password}
          onBlur={handleBlur}
        />
        {touched.password && errors.password}
        <button disabled={!(isValid && dirty) || loading} type="submit">
          Dang nhap
        </button>
      </form>
      <Link href="/signUp">sign up</Link>
    </Layout>
  );
};

export default withApollo(SignInPage);
