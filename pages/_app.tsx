import * as React from 'react';
import useDarkMode from 'use-dark-mode';
import { AppProps } from 'next/app';
import { appWithTranslation } from '../i18n';
import Router from 'next/router';
import { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from '../src/theme';
import NProgress from 'nprogress';
import { RecoilRoot } from 'recoil';

// CSS
import './styles.css';
import 'nprogress/nprogress.css';

NProgress.configure({ showSpinner: false });
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => {
  NProgress.done();
  window.scrollTo(0, 0);
});
Router.events.on('routeChangeError', () => NProgress.done());

const App: React.JSXElementConstructor<AppProps> = ({
  Component,
  pageProps,
}: AppProps) => {
  const [isMounted, setIsMounted] = React.useState(false);
  const darkMode = useDarkMode(false);
  const theme = darkMode.value ? darkTheme : lightTheme;
  React.useEffect(() => {
    setIsMounted(true);
  }, []);
  return (
    <RecoilRoot>
      <ThemeProvider theme={theme}>
        {isMounted && <Component {...pageProps} />}
      </ThemeProvider>
    </RecoilRoot>
  );
};

export default appWithTranslation(App);
