import React from 'react';
import { styled } from '../src/theme';
import { WithTranslation } from 'next-i18next';
import { withTranslation } from '../i18n';

// Components
import ContactItem from '../src/components/ContactItem';

//Layouts
import Layout from '../src/containers/Layout';

const GoogleMap = styled.div`
  height: 200px;
  margin-bottom: 30px;
  iframe {
    width: 100%;
    height: 100%;
    border-radius: 5px;
  }
`;

const Flex = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
`;

const Box = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  flex: 0 0 100%;
  max-width: 100%;
  ${(props) => props.theme.mediaQueries.md} {
    flex: 0 0 50%;
    max-width: 50%;
  }
`;

const ContactPage = ({ t }: WithTranslation) => {
  return (
    <Layout title={t('title')} subTitle={t('subTitle')}>
      <GoogleMap>
        <iframe
          title="address"
          src="https://maps.google.com/maps?q=Linh Chiểu Thủ Đức&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
          frameBorder="0"
          allowFullScreen={true}
        />
      </GoogleMap>
      <Flex>
        <Box>
          <ContactItem icon="icon-phone" name={t('phone')} value="0869123020" />
        </Box>
        <Box>
          <ContactItem
            icon="icon-envelop"
            name="Email"
            value="vietxuandev@gmail.com"
          />
        </Box>
        <Box>
          <ContactItem
            icon="icon-location"
            name={t('address.name')}
            value={t('address.value')}
          />
        </Box>
        <Box>
          <ContactItem
            icon="icon-facebook"
            name="Facebook"
            value="fb.com/vietxuandev"
          />
        </Box>
        <Box>
          <ContactItem
            icon="icon-instagram"
            name="Instagram"
            value="instagram.com/vietxuandev"
          />
        </Box>
        <Box>
          <ContactItem
            icon="icon-earth"
            name="Website"
            value="vietxuandev.com"
          />
        </Box>
      </Flex>
    </Layout>
  );
};

export default withTranslation('contact')(ContactPage);
