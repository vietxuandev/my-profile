import withApollo from 'next-with-apollo';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { getToken } from '../src/token';

const App = ({ Page, props }) => {
  return (
    <ApolloProvider client={props.apollo}>
      <Page {...props} />
    </ApolloProvider>
  );
};

export default withApollo(
  ({ initialState }) => {
    const token = getToken();
    return new ApolloClient({
      uri: 'http://localhost:4000/graphql',
      headers: {
        authorization: token ? `Bearer ${token}` : '',
      },
      cache: new InMemoryCache().restore(initialState || {}),
    });
  },
  {
    render: App,
  }
);
