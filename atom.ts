import { atom } from 'recoil';

type UserState = {
  _id: string;
  username: string;
};

export const userState = atom<UserState>({
  key: 'userState',
  default: undefined,
});
